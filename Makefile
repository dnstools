# This file is part of dnstools
# Copyright (C) 2012, 2014, 2015 Sergey Poznyakoff <gray@gnu.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

SUBDIRS = dnsdbck rpsl2acl axfr2acl vhostcname renewck whoseip nsdbimport dgd

include Make.vars

dist:
	for dir in $(SUBDIRS); do \
	  make -C $$dir dist; \
        done

publish: 
	@s=`for dir in $(SUBDIRS); do \
	  awk -v dir=$$dir \
              'NF==3 && /=>/ { sub(/ *,/,"",$$3); \
                               if ($$1 == "'\''NAME'\''") \
                                 name=$$3;          \
                               else if ($$1 == "'\''VERSION'\''") \
                                 version=$$3; }; \
               END { if (name && version) { \
	               s = name "-" version ".tar.gz"; \
                       gsub(/'\''/, "", s); \
                       print dir, s; \
                     } } ' $$dir/Makefile.PL; \
	done | \
        while read dir file; do ln -f $$dir/$$file .; echo $$file; done`; \
        if test -n "$$s"; then \
          echo $(GNUPLOAD) $$s; \
	  $(GNUPLOAD) $$s; \
	  rm $$s; \
        fi

clean:; @rm -f *.tar.gz *.tar.gz.sig

